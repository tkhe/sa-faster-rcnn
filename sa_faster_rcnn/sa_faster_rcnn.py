import numpy as np
import fvcore.nn.weight_init as weight_init
import torch
from torch import nn
from torch.nn import functional as F

from detectron2.layers import Conv2d, ShapeSpec, get_norm, SimplifiedSelfAttention
from detectron2.modeling.roi_heads.box_head import ROI_BOX_HEAD_REGISTRY


@ROI_BOX_HEAD_REGISTRY.register()
class FastRCNNHead(nn.Module):
    def __init__(self, cfg, input_shape: ShapeSpec):
        super().__init__()

        conv_dim   = cfg.MODEL.ROI_BOX_HEAD.CONV_DIM
        norm       = cfg.MODEL.ROI_BOX_HEAD.NORM

        self._output_size = (input_shape.channels, input_shape.height, input_shape.width)

        self.conv_norm_relus = nn.Sequential(
            Conv2d(
                self._output_size[0],
                conv_dim,
                kernel_size=3,
                padding=1,
                bias=not norm,
                norm=get_norm(norm, conv_dim),
                activation=F.relu
            ),
            Conv2d(
                conv_dim,
                conv_dim,
                kernel_size=3,
                padding=2,
                dilation=2,
                bias=not norm,
                norm=get_norm(norm, conv_dim),
                activation=F.relu
            ),
            Conv2d(
                conv_dim,
                conv_dim,
                kernel_size=3,
                padding=3,
                dilation=3,
                bias=not norm,
                norm=get_norm(norm, conv_dim),
                activation=F.relu
            ),
            Conv2d(
                conv_dim,
                conv_dim,
                kernel_size=3,
                padding=2,
                dilation=2,
                bias=not norm,
                norm=get_norm(norm, conv_dim),
                activation=F.relu
            ),
            Conv2d(
                conv_dim,
                conv_dim,
                kernel_size=3,
                padding=1,
                dilation=1,
                bias=not norm,
                norm=get_norm(norm, conv_dim),
                activation=F.relu
            )
        )
        self._output_size = (conv_dim, self._output_size[1], self._output_size[2])

        self.non_local_block = SimplifiedSelfAttention(conv_dim)

        for layer in self.conv_norm_relus:
            weight_init.c2_msra_fill(layer)

    def forward(self, x):
        x = self.conv_norm_relus(x)
        x = self.non_local_block(x)
        return x

    @property
    def output_size(self):
        return self._output_size


@ROI_BOX_HEAD_REGISTRY.register()
class FastRCNNCascadeHead(nn.Module):
    def __init__(self, cfg, input_shape: ShapeSpec):
        super().__init__()

        conv_dim   = cfg.MODEL.ROI_BOX_HEAD.CONV_DIM
        norm       = cfg.MODEL.ROI_BOX_HEAD.NORM

        self._output_size = (input_shape.channels, input_shape.height, input_shape.width)

        self.conv1 = Conv2d(
            self._output_size[0],
            conv_dim,
            kernel_size=3,
            padding=1,
            bias=not norm,
            norm=get_norm(norm, conv_dim),
            activation=F.relu
        )
        self.conv2 = Conv2d(
            conv_dim,
            conv_dim,
            kernel_size=3,
            padding=2,
            dilation=2,
            bias=not norm,
            norm=get_norm(norm, conv_dim),
            activation=F.relu
        )
        self.conv3 = Conv2d(
            conv_dim,
            conv_dim,
            kernel_size=3,
            padding=2,
            dilation=2,
            bias=not norm,
            norm=get_norm(norm, conv_dim),
            activation=F.relu
        )

        self._output_size = (conv_dim, self._output_size[1], self._output_size[2])

        self.non_local_block = SimplifiedSelfAttention(conv_dim)

        for layer in [self.conv1, self.conv2, self.conv3]:
            weight_init.c2_msra_fill(layer)

    def forward(self, x):
        o1 = self.conv1(x)
        o2 = self.conv2(x)
        o3 = self.conv3(x)
        x = o1 + o2 + o3
        x = self.non_local_block(x)
        return x

    @property
    def output_size(self):
        return self._output_size
